Salle de classe de démonstration basée sur un cursus T.S.S.R. - CFRP AVH

# REAC
L'ensemble des competences et activitées enseignées dans le cadre de la formation TSSR sont définie par un document officiel du Ministère du travail appelé REAC.  
> Referentiel Emploi Activites Competences du titre professionnel de Technicien Supérieur Systèmes et Réseaux

#### Activités types 
Les 3 grands axes du métier de TSSR sont appelés **Activités types**:
1. Assister les utilisateurs en centre de services.
2. Maintenir, exploiter et sécuriser une infrastructure centralisée.
3. Maintenir et exploiter une infrastructure distribuée et contribuer à sa sécurisation.

#### Compétences professionnelles 
Les Activités types sont détaillées en 15 **Compétences Professionnelles**:

1. Mettre en service un équipement numérique.
2. Assister les utilisateurs sur leurs équipements numériques.
3. Gérer les incidents et les problèmes.
4. Assister à l'utilisation des ressources collaboratives.
5. Maintenir et exploiter le réseau local et la téléphonie.
6. Sécuriser les accès à Internet.
7. Maintenir et exploiter un environnement virtualisé.
8. Maintenir et exploiter un domaine ActiveDirectory et les serveurs Windows.
9. Maintenir et exploiter un serveur Linux.
10. Configurer les services de déploiement et de terminaux clients légers.
11. Automatiser les tâches à l'aide de scripts.
12. Maintenir et sécuriser les accès réseaux distants.
13. Superviser l'infrastructure.
14. Intervenir dans un environnement de Cloud Computing.
15. Assurer sa veille technologique.

# Presentation du site  
L'ensemble de ce site est écrit au format Markdown; Il est stocké dans un reférentiel GIT et hebergé sur une appliance Mkdocs.
>Si aucune de ces notions ne vous est familière, consultez le support de cours GIT ci dessous.

Le repo de ce site est accessible et clonable à cette adresse: https://gitlab.com/CFRPAVH/demo.git
Vous avez la possibilité de cloner les infos dans une nouvelle branche, d'y apporter des modifications et d'envoyer votre nouvelle branche sur le repo principale.
Le modérateur pourra ensuite integrer vos modifications au site principal ( ou pas).

> Objectif : construire votre propre dépot de support de cours.

## Referentiel GITLab
```
git clone https://gitlab.com/CFRPAVH/demo.git
cd demo
git branch "votre nom"
git checkout "votre nom"

```

# GIT

## Zones de travail

Il est crucial de commencer par présenter quatre "Zones de travail" ou "Etapes" de Git dans lesquelles nous apprendrons comment ajouter un fichier à un référentiel Git et les étapes necessaires.

### Étape 1 : Le répertoire de travail

Le répertoire de travail est principalement le répertoire *LOCAL* qui contient tous vos fichiers de projet sur votre ordinateur. À ce stade, les modifications peuvent ou non être suivies et gérées par Git.

### Étape 2 : La zone de transit

Vous pouvez appeler cette étape « index Git », et c’est aussi simple qu’une zone intermédiaire qui met en file d’attente vos modifications à un endroit pour la validation suivante. Les fichiers dans la zone intermédiaire sont suivis et gérés par Git ( En anglais Stagged).

### Étape 3 : Le référentiel Git (ou l’historique des validations)

Dans cette étape, Git crée le référentiel local et ajoute le dossier « .git » dans la structure de dossiers du projet, et il contient le référentiel Git réel. Les fichiers validés dans la zone intermédiaire sont déplacés vers le référentiel Git et ajoutés à l’historique de validation.

Les trois étapes ci-dessus représentent votre référentiel Git local. En d’autres termes, jusqu’à l’étape 3, aucun fichier n’a encore été uploadé dans le référentiel distant (GitHub, GitLab, etc.) et voici la quatrième étape.

### Étape 4 : Le référentiel distant

La quatrième étape est la dernière étape d’un flux de travail Git, et c’est essentiellement là que le code sera poussé (Uploadé) vers le référentiel distant sur Internet. Le référentiel distant comporte les trois étapes précédentes en interne.

## Les branches GIT

### C’est quoi une branche Git ?
En général, une branche de développement est une bifurcation de l’état du code qui crée un nouveau chemin pour son évolution. Elle peut être parallèle à d’autres branches Git que vous pouvez générer. Il est ainsi possible d’incorporer de nouvelles fonctionnalités à notre code de manière ordonnée et précise.

L’utilisation des branches Git présente de multiples avantages. Cependant, nous souhaitons souligner les deux suivants :

Il est possible de développer de nouvelles fonctionnalités pour notre application sans entraver le développement dans la branche principale.
Avec les branches Git, il est possible de créer différentes branches de développement qui peuvent converger dans le même dépôt. Par exemple, une branche stable, une branche test et une branche instable.
Naturellement, chaque développeur pourra établir ses propres méthodes avec ses propres avantages en se basant sur l’expérience.

### Comment utiliser une branche Git

Les utilisations des branches Git sont initialement simples comme vous le remarquerez dans les commandes des branches Git. Mais comme pour la plupart des choses, plus vous avez de branches, plus il peut être difficile de les gérer.

Dans un projet Git, nous pouvons visualiser toutes les branches en entrant la commande suivante dans la ligne de commande :
```
git branch
```
Si aucune branche n’est créée, il n’y aura pas de résultat dans le terminal. La création d’une branche est vraiment simple :

```
git branch [nouvelle_branche]
```
Ensuite, nous devons passer à la branche du développement nouvellement créée. Pour ce faire, nous allons exécuter la commande suivante :

```
git checkout [nouvelle_branche]
```
Le résultat nous informera que nous sommes passés à une nouvelle branche. Nous l’avons appelé test, donc :
```
Switched to branch ‘test’
```
Maintenant, dans cette nouvelle branche de développement, nous pouvons créer autant de modifications de code que nous le voulons sans avoir à changer quoi que ce soit dans la principale. Comme nous pouvons le voir, cela permet de garder le programme organisé pour les nouvelles inclusions de code.

Si nous exécutons la commande pour lister à nouveau les branches, nous verrons qu’une nouvelle branche est ajoutée.
```
git branch
```
Il y a quelque chose que nous devons garder à l’esprit si nous voulons créer une nouvelle branche de développement. Tout d’abord, nous devons nous engager dans la branche principale pour que Git comprenne ce qu’est la branche principale (master). Si nous ne le faisons pas, nous aurons une erreur. Donc, d’abord, commettez et ensuite créez les branches de développement.

Si nous voulons retirer une branche du Git, nous pouvons le faire avec la commande suivante :
```
git branch -d [nom_de_la_branche]
```
Cependant, pour ce faire, nous ne devons pas être situés dans la branche que nous voulons supprimer. Dans ce cas, nous devons donc nous déplacer vers la branche principale et, de là, supprimer la branche que nous venons de créer :
```
git checkout master
git branch -d test
```
Enfin, il arrive un moment où nous avons apporté de nombreuses modifications à une branche de développement. Elle devient stable, et nous voulons donc la relier à une autre branche de développement. Pour cela, il y a la commande merge.

Pour commencer, localisez la branche de développement à laquelle la deuxième branche doit être rattachée. Par exemple, nous allons attacher la branche test à la branche master. Ensuite, nous devons nous placer dans la branche master et fusionner avec la commande :
```
git merge [branche]
```
Voila ..., les fonctions de base de la branche Git sont assez simples. Il vous suffit de connaître les principes de base et d’essayer de garder votre gestion propre.


## Tutoriel Git : apprendre à travailler avec Git pas-à-pas

Une fois Git installé sur votre système, vous pouvez utiliser le système de gestion de versions pour gérer vos projets. Comme pour tout autre logiciel, il convient tout d’abord de comprendre les fonctionnalités et les commandes de base afin de pouvoir tirer profit au maximum de l’application. Dans le cadre de notre tutoriel Git complet pour les débutants, nous vous expliquons les principales étapes de mise en place et d’utilisation de Git via l’invite de commande, afin que vous puissiez ensuite créer et gérer votre propre répertoire sans difficulté.
 

### Créer et cloner un répertoire Git
Le répertoire Git est le répertoire central de tout projet géré et constitue la zone commune à tous les participants permettant de régler l’intégralité du contrôle des versions. Votre première étape dans Git consistera donc à créer un tel répertoire principal ou à le cloner (sous la forme d’une copie de travail), dans la mesure où vous vous connectez à un projet dont le développement commun est déjà géré à l’aide de Git.

Si vous souhaitez reconfigurer le contrôle de versions ou si vous venez tout juste d’installer cet outil pour apprendre à utiliser Git, vous devrez tout d’abord créer un répertoire. Pour ce faire, allez dans le répertoire local désiré sur votre appareil à l’aide de « cd » (change directory) :

```
cd chemin de répertoire individuel
```

À cet endroit, saisissez la commande suivante pour créer un répertoire .git :
```
git init
```
Si le répertoire Git existe déjà pour votre projet, vous aurez simplement besoin de l’adresse Web ou réseau de ce répertoire pour 

créer une copie de travail sur votre ordinateur avec la commande « git clone » :

git clone https://gitlab.com/CFRPAVH/demo.git
 Note
Git supporte différents protocoles de transfert. Au lieu de HTTPS utilisé dans l’exemple, vous pouvez notamment utiliser SSH afin d’accéder à un répertoire, à condition que vous disposiez de l’autorisation correspondante. Voir l

Vérifier le statut du répertoire et ajouter de nouveaux fichiers à la gestion de versions
Une bonne organisation du répertoire de travail fait partie des bases essentielles de Git. Ce répertoire vous permet non seulement de proposer vos propres modifications et de nouveaux ajouts à un projet qui sont alors repris via commit (« partage »), mais aussi d’acquérir des informations sur les activités d’autres utilisateurs. Vous pouvez vérifier l’actualité de votre copie de travail à l’aide de la commande suivante :
```
git status
```
En cas de création d’un nouveau répertoire ou de correspondance absolue du répertoire principale et de la copie de travail, vous êtes généralement informé du fait que le projet ne comporte aucune modification (« No commits yet »). Git vous indique par ailleurs que vous n’avez actuellement partagé aucune modification pour le prochain commit (« nothing to commit »).

Afin d’ajouter un nouveau fichier à la gestion des versions ou de signaler un fichier édité pour le prochain commit, utilisez la commande « git add » sur ce fichier (il doit se trouver dans le répertoire de travail). Dans notre tutoriel Git, nous avons ajouté à titre d’exemple un document texte intitulé « Test » :
```
git add Test.txt
```

À présent, si l’on vérifie à nouveau le statut du répertoire, le document servant d’exemple est présenté comme un candidat potentiel pour la prochaine étape de modification officielle du projet (« Changes to be commited ») :

Valider les modifications via commit et les reprendre dans le HEAD
L’ensemble des modifications que vous avez enregistrées pour la gestion des versions (de la façon décrite à la section précédente) doivent toujours être validées par un commit pour être reprises dans le HEAD. Le HEAD est un type d’index qui renvoie au dernier commit ayant pris effet dans l’environnement de travail Git actuel (également appelé « branche »). Pour cette étape, la commande est la suivante :
```
git commit
```
#### Note
Vérifiez toujours avant de saisir la commande si l’ensemble des modifications souhaitées pour le commit sont marquées comme telles (c.-à-d. avec « git add »). Dans le cas contraire, elles seront ignorées même si elles se trouvent dans le répertoire de la copie de travail.

Après l’exécution de la commande, Git lance automatiquement l’éditeur que vous avez enregistré comme choix par défaut lors de l’installation ou que l’outil de gestion de versions prévoit par défaut. Dans le document, vous pouvez à présent saisir un commentaire individuel concernant le commit prévu, sachant que les lignes déjà contenues seront commentées avec un point-virgule et ne seront pas affichées par la suite. Git crée le commit dès que vous fermez l’éditeur :

Après avoir exécuté la commande « git commit », vous obtenez un message récapitulatif portant sur le commit : dans les crochets précédents, vous trouverez d’une part le nom de la branche (branche du projet ; dans le cas présent « master », puisque notre répertoire de travail est également notre répertoire principal), dans lequel les modifications ont été transposées, ainsi que la somme de contrôle SHA 1 du commit (ex « c0fdc90 »). Ils sont suivis par un commentaire choisi librement (exe « modification du .... ») et par les informations concrètes sur les modifications effectuées.

### Éditer ou annuler des commits générés

Si vous avez repris les modifications sous la forme d’un commit, vous pouvez éditer le contenu a posteriori ou le reprendre intégralement. De telles modifications peuvent par exemple s’avérer nécessaires lorsque le commit a été généré trop tôt et que certains fichiers ou certaines modifications essentiels ont été oubliés. Dans un tel cas, il convient de mettre à disposition les fichiers nouveaux ou modifiés a posteriori en utilisant « git add » et de répéter l’enregistrement dans le répertoire principal. Pour ce faire, ajouter l’option --amend à la commande de base :
```
git commit --amend
```
Si vous souhaitez en revanche reprendre le dernier commit généré, vous devrez utiliser la commande Git suivante :
```
git reset --soft HEAD~1
```
Cette commande permet de reprendre le dernier commit enregistré dans le HEAD. Les fichiers qu’il contient sont ainsi réinitialisés au statut « modifications prévues pour le prochain commit ». Si vous souhaitez en revanche supprimer entièrement les données de travail, utilisez la commande suivante à la place de la commande précédente :
```
git reset --hard HEAD~1
```

### Afficher l’historique des commits

Les fonctionnalités élémentaires de versionnage sont une bonne raison d’apprendre la gestion de projet avec Git. L’un des gros atouts de ce système open source réside dans le fait qu’il permet d’afficher à tout moment les dernières modifications effectuées dans le répertoire. La commande Git nécessaire pour y parvenir est :
```
git log
```
La commande « git log » liste les commits générés par ordre anti chronologique sachant que par défaut, la somme de contrôle SHA 1, l’auteur (le nom et l’adresse email) ainsi que la date du commit concerné sont affichés. Par ailleurs, vous trouverez également le message individuel, une note importante qui vous servira ou qui servira aux utilisateurs à classer rapidement les différentes modifications.

La commande log peut par ailleurs être modifiée à l’aide de différents paramètres. Certaines options utiles sont listées dans le tableau ci-après :

Option pour la commande « git log »	Description
-p	indique également les modifications contenues dans un commit
-2	liste uniquement les derniers commits exécutés
--stat	ajoute à chaque entrée une petite statistique indiquant quels fichiers ont été modifiés et combien de lignes ont été ajoutées ou supprimées
--pretty	modifie le format de la version pour l’un des différents formats disponibles ; --pretty=oneline est par exemple un format possible qui liste tous les commits dans une même ligne
--abbrev-commit	affiche uniquement les premiers caractères d’une somme de contrôle SHA 1
--relative-date	affiche la date d’une modification dans un format relatif (par ex. « il y a deux semaines »)

### Reprendre des commits dans le répertoire principal
Jusqu’à présent, nous vous avons montré dans ce tutoriel Git comment enregistrer les modifications sous forme de commit dans le HEAD du répertoire local. Toutefois, pour qu’elles soient reprises dans le répertoire principal, il est nécessaire de saisir la commande suivante :
```
git push origin master
```
Avec ce code, Git transmet automatiquement tous les commits créés, qui jusqu’à présent se trouvaient uniquement dans la copie de travail, dans le répertoire principal également appelé « master ». Si vous remplacez ce nom dans le code indiqué par une autre branche (branche de projet), les fichiers sont alors directement envoyés à cet endroit.

### Balisage : créer, supprimer et lister des balises dans Git

Comme de nombreux autres systèmes de contrôle de versions, Git dispose également d’une fonction de balisage permettant de marquer l’importance de certains points dans l’historique d’un répertoire. Les balises de ce type sont généralement utilisées pour marquer des versions d’un logiciel, par exemple version 1.0, 2.0, etc., pour qu’elles puissent être consultées facilement et à tout moment dans de gros projets. Dans ce cadre, Git supporte deux types de balises :

les balises annotées (« annotated ») sont enregistrées comme des objets indépendants dans la base de données, avec la somme de contrôle individuelle, le message de balisage, la date, le nom et l’adresse email de l’auteur de la balise ainsi que la signature GNU Privacy Guard facultative (signature GPG).
les balises non annotées (« lightweight ») fonctionnent exclusivement comme des références à un commit, de façon similaire aux branches. Ce type peut également être utilisé si vous avez uniquement besoin de balises temporaires ou si vous ne souhaitez pas renseigner des informations étendues.
Dans Git, vous pouvez créer des balises annotées en utilisant la commande « git tag -a » sur le commit souhaité. Si vous y ajoutez également le paramètre « -m », vous pouvez formuler le message de balisage désiré – entre guillemets droits – directement dans la ligne de commande. Dans ce tutoriel Git, nous avons généré le commit « Test » que nous avons également associé à une balise avec le message « example tag » :
```
git tag -a Test -m "example tag"
```
#### Note
Si vous n’utilisez pas le paramètre « -m » lors de la création de la balise, Git ouvre automatiquement l’éditeur afin de vous permettre d’y saisir le message de balisage désiré.

Pour les balises non annotées, il convient de procéder de façon similaire : utilisez simplement la commande de base « git tag » sur le commit souhaité sans autre paramètre. Dans l’exemple de notre tutoriel Git, cela correspondrait à la commande suivante :
```
git tag Test
```
Dès que les balises pour votre répertoire sont disponibles, vous pouvez les afficher à l’aide de la commande « git tag » que vous connaissez déjà et des paramètres facultatifs « -l » ou « --list » :
```
git tag
git tag -l
git tag --list
```

La commande « git tag »-affiche la balise créée au préalable pour l’exemple de commit « Test ».
Pour supprimer une balise du répertoire de travail local, utilisez la suite de commandes « git tag -d » sur la balise en question. Pour supprimer notre référence à « Test », il faut donc procéder comme suit :

```
git tag -d Test
```
À l’instar des commits, le transfert des balises dans le répertoire principal s’effectue également manuellement. Pour ce faire, vous devrez utiliser le nom de la balise et la commande « git push origin ». À la place du nom de la balise, vous pouvez toutefois également ajouter le paramètre « --tags » qui permet d’enregistrer toutes les balises générées dans le répertoire :
```
git push origin --tags
```

### Créer, gérer et supprimer des branches
Les branches déjà évoquées dans ce tutoriel Git ne sont en fait rien d’autre que des versions de travail personnelles du répertoire principal, lui-même qualifié de branche portant le nom de « master ». Avec ces branches de travail, Git offre une base idéale pour développer des fonctionnalités de façon isolée et les rassembler uniquement à un moment ultérieur. Cette dernière action est également appelée « fusionner » (en angl. « merge »).

Créer une nouvelle branche n’a rien de compliqué : vous aurez simplement besoin de la commande « git branch » et d’y rattacher le nom de la branche souhaitée. Vous pouvez par exemple créer une branche portant le nom « test_branch » de la manière suivante :
```
git branch test_branch
```

Vous pouvez ensuite passer à cette branche à tout moment à l’aide de la commande « git checkout » :
```
git checkout test_branch
```

Après avoir changé de branche avec succès, Git affiche le message « Switched to branch … » (fr. « passé à la branche… »).
Si vous souhaitez fusionner des branches, vous devrez utiliser la commande « git merge ». À l’aide de « checkout », rendez-vous dans le répertoire devant accueillir une nouvelle branche et exécutez à cet endroit la commande précitée en indiquant le nom de la branche à enregistrer. Notre version de travail « test_branch » peut par exemple être fusionnée avec le répertoire principal de la façon suivante :
```
git checkout master
git merge test_branch
```
Si vous avez fusionné des branches de travail et que vous n’avez plus besoin d’une branche particulière, vous pouvez la supprimer en toute simplicité. Pour ce faire, utilisez la suite de commandes « git branch -d » sur la branche de la version qui n’est plus nécessaire. L’exemple de notre tutoriel Git « test_branch » peut être supprimé en saisissant ce qui suit :
```
git branch -d test_branch
```
Le seul prérequis pour procéder à la suppression est de se trouver dans une autre branche. Par conséquent, avant d’exécuter la commande, nous sommes repassés dans le répertoire principal comme le montre la capture d’écran suivante :

Changement et suppression de branche à l’aide de Git-Bash

Git valide la suppression d’une branche avec le message « Deleted branch … »
